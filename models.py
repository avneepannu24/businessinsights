from sqlalchemy import create_engine
import pandas as pd
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy.types import Integer, String, Float
from logs import LoggerFactory
logger = LoggerFactory.get_logger("Notifying", log_level="INFO")

Server = 'VISUAL'
Database = 'BIREPORTS'
Driver = 'ODBC Driver 17 for SQL Server'


def fetch_data(user):
    logger.info("Fetching data from db...")
    Server = 'Visual\play'
    Database = 'PMKPLAY'
    Driver = 'ODBC Driver 17 for SQL Server'
    Database_Con = f'mssql://AVNEETP:Pmk$0913@{Server}/{Database}?driver={Driver}'
    conn = None
    try:
        engine2 = create_engine(Database_Con)
        conn = engine2.connect()
        connection = conn.execution_options(
            stream_results=True)
        logger.info("Connected successfully to PMK db...")

        sql = """
                                SELECT po_line.[ROWID]
                                  ,po_line.[PURC_ORDER_ID]
                                  ,po_line.[LINE_NO]
                                  ,po_line.[PART_ID]
                                  ,po.[VENDOR_ID]
                                  ,po.[BUYER]
                                  ,po_line.[VENDOR_PART_ID]
                                  ,po_line.[ORDER_QTY]
                                  ,po_line.[TOTAL_AMT_ORDERED]
                                  ,po_line.[UNIT_PRICE]
                                  ,po.[ORDER_DATE]
                                  ,po_line.[DESIRED_RECV_DATE]
                                  ,po.[STATUS]
                                  ,po.[CREATE_DATE]

                              FROM [PMKPLAY].[dbo].[PURC_ORDER_LINE] as po_line 
                              LEFT JOIN [PMKPLAY].[dbo].[PURCHASE_ORDER] as po
                              ON po_line.PURC_ORDER_ID = po.ID
                              WHERE po.[ORDER_DATE] >= '2020-01-01'
                              ORDER BY po_line.[ROWID] DESC
                                """
        orig_df = []
        origin_df = pd.DataFrame()

        for chunk_dataframe in pd.read_sql(sql, conn, chunksize=500):
            orig_df.append(chunk_dataframe)
            print(f"Got dataframe w/{len(chunk_dataframe)} rows")

        origin_df = pd.concat(orig_df, ignore_index=True)
        return origin_df
    except Exception as e:
        logger.error("Error occured on DB call.", e)
    finally:
        conn.close()
        logger.info("DB connection successfully closed.")

def fetch_parts_site_data():
    logger.info("Fetching data from parts site ...")
    Server = 'Visual\play'
    Database = 'PMKPLAY'
    Driver = 'ODBC Driver 17 for SQL Server'
    Database_Con = f'mssql://AVNEETP:Pmk$0913@{Server}/{Database}?driver={Driver}'
    conn = None
    try:
        engine2 = create_engine(Database_Con)
        conn = engine2.connect()
        connection = conn.execution_options(
            stream_results=True)
        logger.info("Connected successfully to PMK db...")

        sql = """
                                SELECT [ROWID]
                                  ,[SITE_ID]
                                  ,[PART_ID]
                                  ,[PLANNING_LEADTIME]
                                  ,[PRODUCT_CODE]
                                  ,[FABRICATED]
                                  ,[PURCHASED]
                                  ,[STOCKED]
                                  ,[ABC_CODE]
                                  ,[PREF_VENDOR_ID]
                                  ,[UNIT_MATERIAL_COST]
                                  ,[STATUS]
                                  ,[MRP_EXCEPTION_INFO]
                                  ,[MULTIPLE_ORDER_QTY]
                                  ,[CREATE_DATE]
                                  ,[MODIFY_DATE]
                                  ,[IS_RATE_BASED]
                              FROM [PMKPLAY].[dbo].[PART_SITE]
                              WHERE [CREATE_DATE] >= '2021-01-01'
                                """
        orig_df = []
        origin_df = pd.DataFrame()

        for chunk_dataframe in pd.read_sql(sql, conn, chunksize=500):
            orig_df.append(chunk_dataframe)
        origin_df = pd.concat(orig_df, ignore_index=True)
        return origin_df
    # todo handle driver not found error
    except Exception as e:
        logger.error("Error occurred on DB call.", e)
    finally:
        conn.close()
        logger.info("DB connection successfully closed.")

def mssql_engine(user='powerbi', password='KZ$0822', host='VISUAL', db='BIREPORTS'):
    engine = create_engine(f'mssql://powerbi:KZ$0822@{Server}/{Database}?driver={Driver}')
    return engine

def get_insights_table_from_db():
    insights_table_from_db = pd.read_sql_table('tempbi', mssql_engine())
    return insights_table_from_db


def connect_powerbi(df):
    table_name = 'tempbi'
    df = df[
        ['insight_type', 'insight_text', 'buyer', 'part_id', 'best_vendor', 'best_price', 'vendor_on_po', 'future_po',
         'current_po', 'UserAction', 'UserFeedback']]
    print("df.columns")
    print(df.columns)
    # function to overwrite table in db using df
    df.to_sql(
        table_name,
        mssql_engine(),
        if_exists='replace',
        index=False
    )
