import pandas as pd
from logs import LoggerFactory


logger = LoggerFactory.get_logger("Notifying",log_level ="INFO")

def get_vendor_df_with_max_spend(original_df, enteredBuyer):
    buyer_df = get_buyer_df(original_df, enteredBuyer)
    group_by_amount = buyer_df.groupby(['VENDOR_ID'])['Total Spend'].sum().reset_index(name='TotalSpendByVendor')
    vendor_with_max_spend = group_by_amount[
        group_by_amount['TotalSpendByVendor'] == group_by_amount['TotalSpendByVendor'].max()]
    max_spend_vendor = vendor_with_max_spend['VENDOR_ID'].values[0]
    max_amount = round((vendor_with_max_spend['TotalSpendByVendor'].values[0]), 2)
    max_spend_vendor_df = original_df[original_df['VENDOR_ID'] == max_spend_vendor]
    return max_spend_vendor_df

def get_buyer_df(original_df, enteredBuyer):
    df_Firmed_status = get_df_firmed_status(original_df)
    buyer_df = df_Firmed_status[df_Firmed_status['BUYER'] == enteredBuyer]
    return buyer_df

def get_df_firmed_status(original_df):
    df_Firmed_status = original_df[original_df['STATUS'] != 'C']
    df_Firmed_status = df_Firmed_status[df_Firmed_status['STATUS'] != 'X']
    df_Firmed_status = df_Firmed_status[df_Firmed_status['STATUS'] != 'R']
    return df_Firmed_status


def fill_data_for_parts_insight(enteredBuyer,lower_price_vendor,part_id,unit_price,future_po,current_po,kwargs):
    kwargs['insight_type'].append("Preferred Vendor for Part ID")
    kwargs['buyer'].append(enteredBuyer)
    kwargs['best_vendor'].append(lower_price_vendor)
    kwargs['part_id'].append(part_id)
    kwargs['best_price'].append(unit_price)
    kwargs['future_po'].append(future_po)
    kwargs['current_po'].append(current_po)
    kwargs['vendor_on_po'].append(lower_price_vendor)

def process_df(enteredBuyer, df_insight, serial, **kwargs):
    if serial == 1:
        if (len(df_insight['po_list'].values[0]) >= 1):
            po_lst = df_insight['po_list'].values[0]
            msg = "To reduce administration work, consolidate these PO/s {} with current PO {}. Since they are from the same vendor {}.".format(
                po_lst, str(kwargs['current_po']), str(df_insight['VENDOR_ID'].values[0]))
            logger.info("[Consolidate POs] %s", msg)
            kwargs['buyer'].append(enteredBuyer)
            kwargs['insight_text'].append(msg)
            kwargs['vendor_on_po'].append(df_insight['VENDOR_ID'].values[0])
            kwargs['best_vendor'].append(df_insight['VENDOR_ID'].values[0])
            kwargs['best_price'].append("")
            kwargs['part_id'].append("")
            kwargs['future_po'].append(str(po_lst))
            kwargs['insight_type'].append("Consolidate POs")

    elif serial == 3:
        for index in range(len(df_insight)):
            current_po = kwargs.get('po')
            part_id = df_insight['part_id'][index]
            vendor_id = df_insight['vendor_id'][index]
            unit_price = df_insight['unit_price'][index]
            new_unit_price = df_insight['new_unit_price'][index]
            lower_price_vendor = df_insight['preferred_vendor'][index]
            new_preferred_vendor = df_insight['new_preferred_vendor'][index]

            other_vendors = [*filter(lambda i: i != lower_price_vendor, df_insight['vendor_id'][index])]
            other_vendors_list = str(other_vendors)[1:-1]
            future_po = ""
            old_pos_list = [*filter(lambda i: i != future_po, df_insight['old_po_list'][index])]
            old_pos = str(old_pos_list)[1:-1]

            if (new_unit_price and new_preferred_vendor and unit_price > new_unit_price):
                if(lower_price_vendor.casefold() == new_preferred_vendor.casefold()):
                    msg = "According to parts maintenance data, you can get this part {} at better price of ${} from the same vendor {}.".format(part_id, new_unit_price, new_preferred_vendor)
                    kwargs['insight_text'].append(msg)
                    kwargs['insight_type'].append("Preferred Vendor for Part ID")
                    fill_data_for_parts_insight(enteredBuyer, lower_price_vendor, part_id, new_unit_price, future_po,
                                                current_po, kwargs)


                else: # lower_price_vendor != new_preferred_vendor
                    msg = "According to parts maintenance data, you can get this part {} at better price of ${} from the vendor {}.".format(part_id, new_unit_price, new_preferred_vendor)
                    kwargs['insight_text'].append(msg)
                    kwargs['insight_type'].append("Preferred Vendor for Part ID")
                    current_po = df_insight['purchase_order_id'][index]
                    fill_data_for_parts_insight(enteredBuyer, lower_price_vendor, part_id, new_unit_price, future_po,
                                                current_po, kwargs)


            elif(len(vendor_id) > 1 and lower_price_vendor not in vendor_id):
               msg = "You can get a better price of ${} for part {} if you buy from vendor {} which is less than what you pay when you buy from vendor/s {} on PO/s {}".format(
                unit_price, part_id, lower_price_vendor, other_vendors_list, old_pos_list)
               kwargs['insight_text'].append(msg)
               kwargs['insight_type'].append("Preferred Vendor for Part ID")
               fill_data_for_parts_insight(enteredBuyer, lower_price_vendor, part_id, unit_price, future_po,
                                           current_po, kwargs)

def get_insights(enteredBuyer, po, vendor, original_df, df_Firmed_status, parts_site_df, serial, **kwargs):
    buyer_df_non_firm = original_df[(original_df['BUYER']).str.casefold() == enteredBuyer.casefold()]
    if serial == 1:
        kwargs['current_po'].append(po)
        group_data_by_po_vendor = df_Firmed_status.groupby(['VENDOR_ID'])['PURC_ORDER_ID'].unique().apply(list).reset_index(
            name='po_list')
        firmed_po_list = group_data_by_po_vendor[group_data_by_po_vendor['VENDOR_ID'] == vendor]
        po_lst_length = 0
        if(len(firmed_po_list['po_list'])!=0):
            if(po in firmed_po_list['po_list'].values[0]):
                firmed_po_list['po_list'].values[0].remove(po)
            po_lst_length = len(firmed_po_list['po_list'].values[0])
        if po_lst_length < 1:
            logger.info("Buyer: %s don't have any firmed PO in the bucket", enteredBuyer)
            return 0
        else:
            logger.info('List of firmed POs in the order bucket list.', list(firmed_po_list['po_list'].values))
            process_df(enteredBuyer, firmed_po_list, serial, **kwargs)
    elif serial == 2:
        vendors = []
        first_half = []
        second_half = []
        difference = []
        pct_change = []
        for vendor_id in original_df['VENDOR_ID'].unique():
            df_by_vendor = original_df[original_df['VENDOR_ID'] == vendor_id]
            df_by_vendor = df_by_vendor[df_by_vendor['TOTAL_AMT_ORDERED'] > 0.5]

            # check is vendor passed the criteria, that we have atleast 6 months data with him
            eligibility = eligible_vendor(df_by_vendor)
            if eligibility:
                # compute average of three months
                df = compute_half(df_by_vendor)
                vendors.append(vendor_id)
                df['month'] = df['ORDER_DATE'].dt.month.astype('int')
                for colName, colData in df.iteritems():
                    if (colName == "Total_Semi_Annual_amt"):
                        first_half.append(colData.values[0])
                        second_half.append(colData.values[1])
                        diff = colData.values[1] - colData.values[0]
                        difference.append(diff)
                        pct = (diff / colData.values[0]) * 100
                        pct_change.append(pct)
            else:
                continue

        computational_df = pd.DataFrame(list(zip(vendors, first_half, second_half, difference, pct_change)),
                                        columns=['vendors', 'first_half', 'second_half', 'difference', 'pct_change'])

        print(computational_df)
        final_result = computational_df[computational_df['pct_change'] == computational_df['pct_change'].max()]
        if (len(final_result) > 0):
            insight_msg = "Over the last six months you have increased your average spending with vendor {} from ${} to  ${}. This could be good leverage for negotiations.".format(
                final_result['vendors'].values[0], str(round(final_result['first_half'].values[0], 2)),
                str(round(final_result['second_half'].values[0], 2)))
            kwargs['insight_type'].append("Max spend with vendor")
            logger.info("[Maximum spend with vendor]:  %s",insight_msg)
            kwargs['insight_text'].append(insight_msg)
            kwargs['part_id'].append("")
            kwargs['best_vendor'].append(final_result['vendors'].values[0])
            kwargs['best_price'].append("")
            kwargs['future_po'].append("")
            kwargs['current_po'].append("")
            kwargs['vendor_on_po'].append("")
            kwargs['buyer'].append(enteredBuyer)
        else:
            logger.info("Do not notify, since there are no new insights.")
            return 0

    elif serial == 3:
        part_id_data = original_df[original_df["PURC_ORDER_ID"] == po]
        if(not part_id_data.empty):
            part_ids_list = list(part_id_data['PART_ID'].unique())
            result_df = get_preferred_vendor_for_part_id(original_df, buyer_df_non_firm, part_ids_list, parts_site_df)
            process_df(enteredBuyer, result_df, serial, **kwargs)
        logger.info("No insights for Preferred Vendor for Part ID")




def compute_half(df_by_vendor):
    mask_zero_amount = df_by_vendor['TOTAL_AMT_ORDERED'] > 0.5
    df_by_vendor = df_by_vendor[df_by_vendor['TOTAL_AMT_ORDERED'] > 0.5]

    vendor_df_quarterly = df_by_vendor.groupby([pd.Grouper(key='ORDER_DATE', freq='6M')])[
        'TOTAL_AMT_ORDERED'].mean().reset_index(name="Total_Semi_Annual_amt")

    return vendor_df_quarterly.tail(2)

def eligible_vendor(df_by_vendor):
    count = df_by_vendor.groupby(df_by_vendor["ORDER_DATE"].dt.month).count()
    index = df_by_vendor.index
    final_count = len(count)
    if final_count >= 4:
        return True
    else:
        return False

def get_preferred_vendor_for_part_id(original_df, buyer_df, part_ids_list, parts_site_df):
    purchase_order_id = []
    part_Ids = []
    vendor_Ids = []
    old_Pos = []
    unit_price = []
    new_unit_price = []
    preferred_vendor = []
    new_preferred_vendor = []
    for part in part_ids_list:
        mask_parts_site = parts_site_df['PART_ID'] == part
        if(not mask_parts_site.empty):
            temp_parts_df = parts_site_df[mask_parts_site]
            if(not temp_parts_df.empty):
                latest_unit_price = temp_parts_df['UNIT_MATERIAL_COST'].iloc[0]
                new_unit_price.append(latest_unit_price)
                latest_preferred_vendor = temp_parts_df['PREF_VENDOR_ID'].iloc[0]
                new_preferred_vendor.append(latest_preferred_vendor)
        mask = original_df['PART_ID'] == part
        tempdf = original_df[mask]
        if(not tempdf.empty):
            tempdf.sort_values('UNIT_PRICE', ascending=True, inplace=True)
            part_Ids.append(part)
            purchase_order_id.append(tempdf['PURC_ORDER_ID'].iloc[0])
            vendor_list = []

            for vendor in tempdf['VENDOR_ID'].unique():
                vendor_list.append(vendor)
            vendor_Ids.append(vendor_list)

            old_po_list = []
            for old_po in tempdf['PURC_ORDER_ID'].unique():
                old_po_list.append(old_po)
            old_Pos.append(old_po_list)
            unit_price.append(tempdf['UNIT_PRICE'].iloc[0])

            preferred_vendor.append(tempdf['VENDOR_ID'].iloc[0])

    aggregated_data = []
    for purchase_order_id, partIds, vendorIds, old_Pos, unit_price,new_unit_price, preferred_vendor, new_preferred_vendor in zip(purchase_order_id, part_Ids,
                                                                                            vendor_Ids, old_Pos,
                                                                                            unit_price,new_unit_price,
                                                                                            preferred_vendor,new_preferred_vendor ):
        aggregated_data.append([purchase_order_id, partIds, vendorIds, old_Pos, unit_price, new_unit_price, preferred_vendor,new_preferred_vendor])

    final_df = pd.DataFrame(aggregated_data,
                            columns=['purchase_order_id', 'part_id', 'vendor_id', 'old_po_list', 'unit_price', 'new_unit_price', 'preferred_vendor',
                                     'new_preferred_vendor'])

    return final_df