import os
import sys
import webbrowser
import tkinter as tk
from multiprocessing import Process
from src.win10toast.win10toast import ToastNotifier
from dash import dcc
import warnings
import dash
from dash import html
from dash import dash_table
import pandas as pd
from kafka import KafkaConsumer
import json
from dash.dependencies import Input, Output, State
from flask import Flask, request
from models import fetch_data, get_insights_table_from_db, connect_powerbi, fetch_parts_site_data
from functions import get_insights
from preprocessing import preprocess_data, preprocess_parts_site
import redis
from time import sleep
from logs import LoggerFactory

app = dash.Dash()
warnings.filterwarnings("ignore")
insights_df_global = pd.DataFrame()
final_df_bi = pd.DataFrame()
noti = ToastNotifier()

logger = LoggerFactory.get_logger("Notifying",log_level ="INFO")

def send_notifications(user, po, vendor):
    i_df = try_notify(user, po, vendor)
    logger.info("Total number of insights generated : %d", len(i_df))
    if len(i_df) == 0:
        logger.info("No new insights generated.")
    else:
        # show notifications by iterating over dataframe.
        for i in range(len(i_df)):
            print(str(i_df['insight_text'][i]))
            print(str(i_df['buyer']))
            file_name = 'C:\\Users\\avneetp\\PycharmProjects\\Dashboard\\promark.ico'
            noti.show_toast('Insight', str(i_df['insight_text'][i]), icon_path=file_name, duration=2)


def try_notify(user, po, vendor):
    df = fetch_data(user)
    parts_df = fetch_parts_site_data()

    if df is not None:
        df_Firmed_status, original_df = preprocess_data(df)
        parts_site_df = preprocess_parts_site(parts_df)
        dict = {'insight_id': [], 'insight_type': [], 'insight_text': [], 'buyer': [], 'part_id': [], 'best_vendor': [],
                'best_price': [], 'vendor_on_po': [], 'future_po': [], 'current_po': []}
        for i in range(1, 5):
            result_message = get_insights(user, po, vendor, original_df, df_Firmed_status, parts_site_df, i, **dict)
            if result_message == 0:
                logger.info("No new insights generated")
            if (i == 4):
                global insights_df_global
                insights_dataframe = consolidate_df(**dict)
                insights_df_global = insights_dataframe.copy()
                return insights_df_global
    else:
        logger.info("Do not notify, since there are no new insights.")
        return pd.DataFrame()


def get_final_df_after_comparison(insights_df_global):
    # get insight table from DB
    global final_df_bi
    insights_from_db = get_insights_table_from_db()

    if (insights_from_db is not None and insights_df_global is not None):
        final_df_bi = pd.concat([insights_df_global, insights_from_db], ignore_index=True, sort=False)
        final_df_bi = pd.concat([insights_from_db, insights_df_global]).drop_duplicates(['insight_text']).reset_index(
            drop=True)
    elif (insights_from_db is not None):
        final_df_bi = insights_from_db
    else:
        final_df_bi = insights_df_global
    return final_df_bi


def action(df_global):
    final_df = get_final_df_after_comparison(df_global)
    if final_df is None:
        logger.info("Do not notify, since there are no new insights.")

    app.layout = html.Div([dash_table.DataTable(
        id='table',
        columns=([{"name": i, "id": i, 'type': table_type(final_df[i])} for i in final_df.columns] +
                 [{"id": "UserAction", "name": " UserAction", 'presentation': 'dropdown'}] +
                 [{"id": "UserFeedback", "name": "UserFeedback", 'presentation': 'dropdown'}]),
        data=final_df.to_dict('records'),
        editable=True,  # user can change data of cell
        dropdown={
            'UserAction': {
                'options': [
                    {'label': 'Taken', 'value': 'Taken'},
                    {'label': 'Not Taken', 'value': 'Not Taken'},
                    {'label': 'Saved for Later', 'value': 'Saved for Later'}
                ]
            },
            'UserFeedback': {
                'options': [
                    {'label': 'Useful', 'value': 'Useful'},
                    {'label': 'Not Useful', 'value': 'Not Useful'},
                    {'label': 'Not Understood', 'value': 'Not Understood'}
                ]
            }
        },
        style_cell={'textAlign': 'left'},
        fixed_columns={'headers': True, 'data': 1},
        style_table={'minWidth': '100%'},

        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(220, 220, 220)',
            }
        ],
        style_header={
            'backgroundColor': 'rgb(210, 210, 210)',
            'color': 'black',
            'fontWeight': 'bold'
        },
        style_data={'border': '1px solid black'},
        filter_action='native',
    ),
        html.Button('Save to sql', id='save_to_sql'),

        html.Div(id='table-dropdown-container'),
        html.Div(id="sql_datatable"),
        # create notification while saving data to db
        html.Div(id='placeholder', children=[]),
        dcc.Store(id="store", data=0),
        dcc.Interval(id='interval', interval=1000),
    ])

    @app.callback(
        [Output('placeholder', 'children'),
         Output("store", "data")],
        Input('save_to_sql', 'n_clicks'),
        # Input('interval','n_intervals')]
        [State('table', 'data'),
         State('store', 'data')]
    )
    def df_to_db(n_clicks, dataset, s):
        output = html.Plaintext("Dumped data into db",
                                style={'color': 'green', 'font-weight': 'bold', 'font-size': 'large'})

        no_output = html.Plaintext("", style={'margin': "0px"})

        input_triggered = dash.callback_context.triggered[0]["prop_id"].split(".")[0]
        if input_triggered == "save_to_sql":
            s = 6
            # connect to db
            df = pd.DataFrame(dataset)
            connect_powerbi(df)
            return output, s
        elif input_triggered == 'interval' and s > 0:
            s = s - 1
            if s > 0:
                return output, s
            else:
                return no_output, s
        elif s == 0:
            return no_output, s


def table_type(df_column):
    if isinstance(df_column.dtype, pd.DatetimeTZDtype):
        return 'datetime',
    elif (isinstance(df_column.dtype, pd.StringDtype) or
          isinstance(df_column.dtype, pd.BooleanDtype) or
          isinstance(df_column.dtype, pd.CategoricalDtype) or
          isinstance(df_column.dtype, pd.PeriodDtype)):
        return 'text'
    elif (isinstance(df_column.dtype, pd.SparseDtype) or
          isinstance(df_column.dtype, pd.IntervalDtype) or
          isinstance(df_column.dtype, pd.Int8Dtype) or
          isinstance(df_column.dtype, pd.Int16Dtype) or
          isinstance(df_column.dtype, pd.Int32Dtype) or
          isinstance(df_column.dtype, pd.Int64Dtype)):
        return 'numeric'
    else:
        return 'any'


def consolidate_df(**kwargs):
    aggregated_data = []

    for insight_type, insight_text, buyer, part_id, best_vendor, best_price, vendor_on_po, future_po, current_po in zip(
            kwargs.get('insight_type'), kwargs.get('insight_text'), kwargs.get('buyer'), kwargs.get('part_id'),
            kwargs.get('best_vendor'), kwargs.get('best_price'), kwargs.get('vendor_on_po'), kwargs.get('future_po'),
            kwargs.get('current_po')):
        aggregated_data.append(
            [insight_type, insight_text, buyer, part_id, best_vendor, best_price, vendor_on_po, future_po, current_po])

    insights_df = pd.DataFrame(aggregated_data,
                               columns=['insight_type', 'insight_text', 'buyer', 'part_id', 'best_vendor', 'best_price',
                                        'vendor_on_po', 'future_po', 'current_po'])

    return insights_df


def streamline():
    consumer = KafkaConsumer('biTestingServer.dbo.PURCHASE_ORDER',value_deserializer=lambda m: json.loads(m.decode('urf-8')),api_version=(0, 10, 2))
    logger.info("Successfully connected to Kafka.")
    for message in consumer:
        data = message.value
        po = data['payload']['after']['ID']
        vendor = data['payload']['after']['VENDOR_ID']
        buyer = data['payload']['after']['BUYER']
        send_notifications(buyer, po, vendor)

app2 = Flask(__name__)

@app2.route('/datafromconsumer/', methods=['POST'])
def upload_data():
    temp_dict={
        'buyer': request.form['buyer'],
        'po': request.form['po'],
        'vendor': request.form['vendor']
    }
    logger.info("Generate insights for %s with PO: %s under vendor: %s", temp_dict['buyer'], temp_dict['po'], temp_dict['vendor'])
    send_notifications(temp_dict['buyer'], temp_dict['po'], temp_dict['vendor'])

    return '', 204

def get_data_from_redis():
    redis_interface = redis.Redis(host='192.168.10.117', port='6379')
    data = json.loads(redis_interface.get('TRIGGERDATA').decode('utf-8'))
    redis_interface.delete('TRIGGERDATA')
    return data


def startp(insights_df_global):
    print(insights_df_global)
    df_global = insights_df_global
    action(df_global)
    # open dashboard
    webbrowser.open('http://127.0.0.1:9999/')
    print("Opened Dashboard")
    app.run_server(port=9999)



def start_notify():
    while True:
        redis_interface = redis.Redis(host='172.21.56.4', port='6379')
        logger.info("Redis connected")
        # key = getkeyfromfile()
        try:
            if redis_interface.exists('TRIGGERDATA'):
                trigger_data = json.loads(redis_interface.get('TRIGGERDATA').decode('utf-8'))
                redis_interface.delete('TRIGGERDATA')
                logger.info("Retrieved real time data from Visual: BUYER: %s, PO: %s, VENDOR: %s", trigger_data['buyer'],trigger_data['po'], trigger_data['vendor'])
                send_notifications(trigger_data['buyer'], trigger_data['po'], trigger_data['vendor'])
                print(insights_df_global)

                root = tk.Tk()
                root.title("Procurement Dashboard")
                file_name = 'C:\\Users\\avneetp\\PycharmProjects\\Dashboard\\promark.ico'
                root.iconbitmap(file_name)
                frame = tk.Frame()
                frame.pack(fill=tk.BOTH, expand=True)

                def clickButton1():
                    p1 = Process(target=startp, args=(insights_df_global,))
                    p1.start()
                    root.destroy()

                def clickButton2():
                    root.destroy()
                label = tk.Label(frame, text="Do you want to open a Dashboard?")
                button1 = tk.Button(frame, text="Yes", command=clickButton1)
                button2 = tk.Button(frame, text="No", command=clickButton2)
                label.pack()
                button1.pack()
                button2.pack()
                root.mainloop()
            else:
                logger.info("Didn't get any data, so thread is under sleep state.")
                sleep(1)
        except:
            pass

if __name__ == '__main__':
    try:
        start_notify()
    except KeyboardInterrupt:
        logger.info('Exiting from the Notification System.')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)







